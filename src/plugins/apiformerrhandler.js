const ApiFormErrHandler = {
  // eslint-disable-next-line
  install(Vue, options) {
    Vue.mixin({
      data() {
        return {
          non_field_errors: undefined,
          form_state: {},
          form_invalid_feedback: {}
        };
      },
      created() {
        if (this.form) {
          this.$resetErrFedback();
        }
      },
      methods: {
        $resetErrFedback() {
          let form_invalid_feedback = Object.assign(
            {},
            this.form_invalid_feedback
          );
          let form_state = Object.assign({}, this.form_state);
          for (const attr in this.form) {
            form_invalid_feedback[attr] = null;
            form_state[attr] = null;
          }
          this.form_state = Object.assign({}, form_state);
          this.form_invalid_feedback = Object.assign({}, form_invalid_feedback);
        },
        $putErrFeedback(payload) {
          this.$resetErrFedback();
          if (!payload.data) {
            return false;
          }
          if ("non_field_errors" in payload.data) {
            this.non_field_errors = payload.data.non_field_errors;
          }
          let handled = false;
          for (const attr in payload.data) {
            if (attr in this.form) {
              let messages = "";
              if (payload.data[attr][0]) {
                payload.data[attr].forEach(err_msg => {
                  messages += err_msg;
                });
                this.form_state[attr] = false;
                this.form_invalid_feedback[attr] = messages;
                handled = true;
              }
            }
          }
          return handled;
        }
      }
    });
  }
};

export default ApiFormErrHandler;
