export function ValidateJWT(jwt) {
  try {
    // TODO: los jwt malformados no son validados aquí
    var base64Url = jwt.split(".")[1];
    var base64 = base64Url.replace("-", "+").replace("_", "/");
    var decoded = JSON.parse(window.atob(base64));
  } catch (e) {
    // jwt no válido
    return false;
  }

  var exp = decoded.exp;
  var now = new Date(Date.now());
  var expires = new Date(exp * 1000);
  var diff = (expires - now) / 1000 / 60;

  // jwt expirado o cerca de expirar
  if (diff <= 3) {
    return false;
  }
  // jwt válido
  return true;
}
