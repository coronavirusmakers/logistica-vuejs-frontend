export const ROLE_PRODUCER = 0;
export const ROLE_CONSUMER = 1;
export const ROLE_LOGISTICS = 3;
export const ROLE_CARRIER = 4;
