import Vue from "vue";
import Vuex from "vuex";
import utils from "./modules/utils";
import person from "./modules/person";
import shipping from "./modules/shipping";
import apphealth from "./modules/apphealth";
import auth from "./modules/auth";

Vue.use(Vuex);

export function createStore() {
  return new Vuex.Store({
    modules: {
      utils,
      shipping,
      person,
      apphealth,
      auth
    }
  });
}
