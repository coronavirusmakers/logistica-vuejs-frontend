import Vue from "vue";
import CustomCov19Http from "@/plugins/customhttp";
Vue.use(CustomCov19Http);
let cross = new Vue();

export default {
  namespaced: true,
  state: {
    person: undefined,
    persons: undefined,
    availRegions: null,
    lastres: undefined
  },
  actions: {
    LOAD_LOGISTIC_PERSON_REGIONS: ({ commit }) => {
      const uri = cross.$parseurl(
        "/logistica/middle/person/{person_pk}/region/",
        {
          person_pk: localStorage.getItem("pk_person")
        }
      );
      commit("MUTATE_LOGISTIC_PERSON_REGIONS", []);
      return new Promise((resolve, reject) => {
        cross.$fetchget(uri).then(res => {
          res.json().then(data => {
            if (res.ok) {
              resolve({ res, data });
              commit("MUTATE_LOGISTIC_PERSON_REGIONS", data.results);
            } else {
              reject({ res, data });
              commit("MUTATE_LOGISTIC_PERSON_REGIONS", []);
            }
          });
        });
      });
    },
    LOAD_LOGISTIC_PERSONS: ({ commit }) => {
      const uri = cross.$parseurl("/logistica/basic/person/", undefined, {
        role: 3
      });
      return new Promise((resolve, reject) => {
        cross.$fetchget(uri).then(res => {
          res.json().then(data => {
            if (res.ok) {
              resolve({ res, data });
              commit("MUTATE_LOGISTIC_PERSONS", data.results);
            } else {
              reject({ res, data });
            }
          });
        });
      });
    },
    SWITCH_LOGISTICS_PERSON: ({ commit }, person) => {
      return new Promise(resolve => {
        commit("MUTATE_SET_PERSON", person);
        resolve(person);
      });
    },
    CREATE_MIDDLE_PERSON: ({ commit }, payload) => {
      return new Promise((resolve, reject) => {
        cross
          .$fetchpost("/logistica/middle/person/", {
            body: payload.data
          })
          .then(res => {
            res.json().then(data => {
              if (res.ok) {
                commit("MUTATE_LAST_RES", res);
                resolve({ res, data });
              } else {
                commit("MUTATE_LAST_RES", res);
                reject({ res, data });
              }
            });
          });
      });
    },
    UPDATE_MIDDLE_PERSON: ({ commit }, payload) => {
      return new Promise((resolve, reject) => {
        const uri = cross.$parseurl("/logistica/middle/person/{person_pk}/", {
          person_pk: payload.person_pk
        });
        cross
          .$fetchput(uri, {
            body: payload.data
          })
          .then(res => {
            res.json().then(data => {
              if (res.ok) {
                commit("MUTATE_LAST_RES", res);
                resolve({ res, data });
              } else {
                commit("MUTATE_LAST_RES", res);
                reject({ res, data });
              }
            });
          });
      });
    },
    CREATE_MIDDLE_FOREIGNPERSON: ({ commit }, payload) => {
      return new Promise((resolve, reject) => {
        const uri = cross.$parseurl(
          "/logistica/middle/person/{person_pk}/foreignperson/",
          {
            person_pk: payload.person_pk
          }
        );
        cross
          .$fetchpost(uri, {
            body: payload.data
          })
          .then(res => {
            res.json().then(data => {
              if (res.ok) {
                commit("MUTATE_LAST_RES", res);
                resolve({ res, data });
              } else {
                commit("MUTATE_LAST_RES", res);
                reject({ res, data });
              }
            });
          });
      });
    },
    UPDATE_MIDDLE_FOREIGNPERSON: ({ commit }, payload) => {
      return new Promise((resolve, reject) => {
        const uri = cross.$parseurl(
          "/logistica/middle/person/{person_pk}/foreignperson/{foreignperson_pk}/",
          {
            person_pk: payload.person_pk,
            foreignperson_pk: payload.foreignperson_pk
          }
        );
        cross
          .$fetchput(uri, {
            body: payload.data
          })
          .then(res => {
            res.json().then(data => {
              if (res.ok) {
                commit("MUTATE_LAST_RES", res);
                resolve({ res, data });
              } else {
                commit("MUTATE_LAST_RES", res);
                reject({ res, data });
              }
            });
          });
      });
    },
    FLUSH_DATA: ({ commit }) => {
      commit("MUTATE_FLUSH_DATA");
    }
  },
  mutations: {
    MUTATE_FLUSH_DATA: state => {
      state.person = undefined;
      state.persons = undefined;
      state.availRegions = undefined;
    },
    MUTATE_SET_PERSON: (state, person) => {
      state.person = Object.assign({}, person);
      localStorage.setItem("pk_person", person.pk);
    },
    MUTATE_LAST_RES: (state, res) => {
      state.lastres = res;
    },
    MUTATE_LOGISTIC_PERSONS: (state, results) => {
      if (results[0]) {
        state.person = { ...results[0] };
        localStorage.setItem("pk_person", results[0].pk);
        state.persons = results;
      } else {
        state.person = undefined;
        localStorage.removeItem("pk_person");
      }
    },
    MUTATE_LOGISTIC_PERSON_REGIONS: (state, results) => {
      state.availRegions = [...results];
    }
  },
  getters: {
    person: state => state.person,
    availRegions: state => () => state.availRegions
  }
};
