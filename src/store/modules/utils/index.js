import Vue from "vue";
import CustomCov19Http from "@/plugins/customhttp";
Vue.use(CustomCov19Http);
let cross = new Vue();

export default {
  namespaced: true,
  state: {
    availRoles: undefined,
    mappedAvailRoles: [],
    availShippingStatuses: undefined,
    mappedAvailShippingStatuses: [],
    availResources: undefined,
    mappedAvailResources: [],
    availRequestStatuses: undefined,
    mappedAvailRequestStatuses: [],
    mappedRoleDisambiguation: []
  },
  actions: {
    LOAD_ROLE_DISAMBIGUATION: ({ commit }) => {
      // TODO: MAKE IT IN ONE CALL
      [0, 1, 3, 4, 8].forEach(role => {
        cross
          .$fetchget(`/logistica/basic/role/${role}/disambiguation/`)
          .then(res => {
            res.json().then(data => {
              if (res.ok) {
                commit("MUTATE_ROLE_DISAMBIGUATION", data.results);
              } else {
                // TODO: ERROR?
              }
            });
          });
      });
    },
    LOAD_AVAIL_ROLES: ({ commit }) => {
      return cross.$fetchget("/logistica/basic/role/").then(res => {
        res.json().then(data => {
          if (res.ok) {
            commit("MUTATE_AVAIL_ROLES", data.results);
          } else {
            // TODO: ERROR?
          }
        });
      });
    },
    LOAD_AVAIL_SHIPPING_STATUSES: ({ commit }) => {
      const uri = cross.$parseurl(
        "/logistica/middle/person/{person_pk}/shippingstatus/",
        { person_pk: localStorage.getItem("pk_person") }
      );
      cross.$fetchget(uri).then(res => {
        res.json().then(data => {
          if (res.ok) {
            commit("MUTATE_AVAIL_SHIPPING_STATUSES", data.results);
          } else {
            // TODO: ERROR?
          }
        });
      });
    },
    LOAD_AVAIL_RESOURCES: ({ commit }) => {
      const uri = cross.$parseurl("/logistica/basic/resource/");
      cross.$fetchget(uri).then(res => {
        res.json().then(data => {
          if (res.ok) {
            commit("MUTATE_AVAIL_RESOURCES", data.results);
          } else {
            // TODO: ERROR?
          }
        });
      });
    },
    LOAD_AVAIL_REQUEST_STATUSES: ({ commit }) => {
      const uri = cross.$parseurl(
        "/logistica/middle/person/{person_pk}/requeststatus/",
        { person_pk: localStorage.getItem("pk_person") }
      );
      cross.$fetchget(uri).then(res => {
        res.json().then(data => {
          if (res.ok) {
            commit("MUTATE_AVAIL_REQUEST_STATUSES", data.results);
          } else {
            // TODO: ERROR?
          }
        });
      });
    }
  },
  mutations: {
    MUTATE_ROLE_DISAMBIGUATION: (state, results) => {
      results.forEach(element => {
        state.mappedRoleDisambiguation[element.pk] = element;
      });
    },
    MUTATE_AVAIL_ROLES: (state, results) => {
      state.availRoles = results;
      let mapped = {};
      results.forEach(element => {
        mapped[element.pk] = element.name;
      });
      state.mappedAvailRoles = Object.assign({}, mapped);
    },
    MUTATE_AVAIL_SHIPPING_STATUSES: (state, results) => {
      state.availShippingStatuses = results;
      let mapped = {};
      results.forEach(element => {
        mapped[element.pk] = element.name;
      });
      state.mappedAvailShippingStatuses = Object.assign({}, mapped);
    },
    MUTATE_AVAIL_RESOURCES: (state, results) => {
      state.availResources = results;
      let mapped = {};
      results.forEach(element => {
        mapped[element.pk] = element.name;
      });
      state.mappedAvailResources = Object.assign({}, mapped);
    },
    MUTATE_AVAIL_REQUEST_STATUSES: (state, results) => {
      state.availRequestStatuses = results;
      let mapped = {};
      results.forEach(element => {
        mapped[element.pk] = element.name;
      });
      state.mappedAvailRequestStatuses = Object.assign({}, mapped);
    }
  },
  getters: {}
};
