import Vue from "vue";
import CustomCov19Http from "@/plugins/customhttp";
Vue.use(CustomCov19Http);
let cross = new Vue();

export default {
  namespaced: true,
  state: {
    savedShippings: [],
    lastres: undefined
  },
  actions: {
    LOGISTICA_MIDDLE_PERSON_REQUEST_SHIPPING_CREATE: ({ commit }, payload) => {
      const uri = cross.$parseurl(
        "/logistica/middle/person/{person_pk}/request/{request_pk}/shipping/",
        {
          person_pk: payload.person_pk,
          request_pk: payload.request_pk
        }
      );
      return new Promise((resolve, reject) => {
        cross
          .$fetchpost(uri, {
            body: payload.data
          })
          .then(res => {
            res.json().then(data => {
              if (res.ok) {
                commit("MUTATE_SAVED_SHIPPINGS", data);
                resolve(data);
              } else {
                reject(data);
              }
            });
          });
      });
    },
    DELETE_AUTOMOVEMENT: ({ commit }, payload) => {
      const uri = cross.$parseurl(
        "/logistica/middle/person/{person_pk}/automovementconfig/{automovementconfig_pk}/",
        {
          person_pk: payload.person_pk,
          automovementconfig_pk: payload.automovementconfig_pk
        }
      );
      return new Promise((resolve, reject) => {
        cross
          .$fetchdelete(uri, {
            body: payload.data
          })
          .then(res => {
            res
              .json()
              .then(data => {
                if (res.ok) {
                  commit("MUTATE_LAST_RES", res);
                  resolve(data);
                } else {
                  commit("MUTATE_LAST_RES", res);
                  reject(data);
                }
              })
              .catch(() => {
                // el fallo puede deberse a que un request DELETE no
                // devuelve json y el parseo falla
                if (res.ok) {
                  resolve({ res });
                } else {
                  reject({ res });
                }
              });
          });
      });
    },
    PATCH_SHIPPING: ({ commit }, payload) => {
      const uri = cross.$parseurl(
        "/logistica/middle/person/{person_pk}/shipping/{shipping_pk}/",
        {
          person_pk: payload.person_pk,
          shipping_pk: payload.shipping_pk
        }
      );
      return new Promise((resolve, reject) => {
        cross
          .$fetchpatch(uri, {
            body: payload.data
          })
          .then(res => {
            res
              .json()
              .then(data => {
                if (res.ok) {
                  commit("MUTATE_LAST_RES", res);
                  resolve({ res, data });
                } else {
                  commit("MUTATE_LAST_RES", res);
                  reject({ res, data });
                }
              })
              .catch(() => {
                reject({ res, undefined });
              });
          });
      });
    }
  },
  mutations: {
    MUTATE_LAST_RES: (state, res) => {
      state.lastres = res;
    },
    MUTATE_SAVED_SHIPPINGS: (state, data) => {
      state.savedShippings.push(data);
    }
  },
  getters: {}
};
