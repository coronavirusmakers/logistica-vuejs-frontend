module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: ["plugin:vue/essential", "eslint:recommended"],
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
    indent: ["warn", 2],
    "vue/html-indent": "warn",
    "vue/html-self-closing": [
      "warn",
      {
        html: {
          void: "always",
          normal: "never",
          component: "any",
        },
      },
    ],
    "vue/html-closing-bracket-spacing": [
      "warn",
      {
        selfClosingTag: "always",
      },
    ],
    "no-unused-vars": "warn",
    "no-extra-semi": "warn",
    "vue/mustache-interpolation-spacing": "warn",
    "vue/no-multi-spaces": "warn",
    "vue/require-default-prop": "off", // TODO: elevarlo a "error" en algún momento
    "vue/require-prop-types": "warn",
    "vue/attributes-order": "off",
    "vue/html-quotes": "warn",
    "vue/no-confusing-v-for-v-if": "warn",
    "vue/order-in-components": "off", // TODO: elevarlo a "error" en algún momento
    "vue/this-in-template": "warn",
    "vue/no-v-html": "warn", // TODO: elevarlo a "error" en algún momento
    "vue/script-indent": "warn",
    "vue/max-attributes-per-line": [
      "warn",
      {
        singleline: 10,
        multiline: {
          max: 1,
          allowFirstLine: false,
        },
      },
    ],
    "vue/attribute-hyphenation": "off",
    "comma-dangle": ["error", "never"],
    // "comma-dangle": [
    //   "warn",
    //   {
    //     arrays: "never",
    //     objects: "never",
    //     imports: "never",
    //     exports: "never",
    //     functions: "never",
    //   },
    // ],
  },
  parser: "vue-eslint-parser",
  parserOptions: {},
};
